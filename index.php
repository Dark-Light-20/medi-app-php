<?php
session_start();
if (isset($_SESSION["id"])) header("location: http://".$_SERVER['HTTP_HOST']."/mediapp/php/consult.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/mediapp/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/mediapp/public/css/style.css">
    <title id="title">Medi-App</title>
</head>
<body class="primary-color-light">
    <!--Navbar-->
    <nav class="navbar navbar-light navbar-1 primary-color">
        <!-- Navbar brand -->
        <img src="/mediapp/public/img/logo_transparent.png" width="80" height="80" class="d-inline-block align-top" alt="" loading="lazy">
        <span class="navbar-brand"><h1><span class="badge badge-pill badge-light" id="navbar-title">Medi-APP</span></h1></span>
    </nav>
    <!--/.Navbar-->
    <div class="container-fluid" id="container"></div>
    <script src="/mediapp/public/js/jquery-3.5.1.min.js"></script>
    <script src="/mediapp/public/js/popper.min.js"></script>
    <script src="/mediapp/public/js/bootstrap.min.js"></script>
    <script src="/mediapp/public/js/index.js"></script>
</body>
</html>
