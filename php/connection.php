<?php
function connect(){
    // Get BD-Credentials
    $BD_DATA = file_get_contents('../../../creds/key');
    $BD_DATA = json_decode($BD_DATA);
    // Connection
    $conn = new mysqli("localhost", $BD_DATA->user, $BD_DATA->pswd, $BD_DATA->bd);
    if($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $conn->set_charset("utf8");
    return $conn;
}
?>
