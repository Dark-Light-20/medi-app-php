<?php
if (isset($_POST["username"]) && isset($_POST["password1"]) && isset($_POST["password2"])) {
    $user = $_POST["username"];
    $pss1 = $_POST["password1"];
    $pss2 = $_POST["password2"];
    if ($pss1 != "" && $pss1 == $pss2) {
        $pss1 = password_hash($pss1, PASSWORD_DEFAULT);
        // Connection
        include_once("./connection.php");
        $conn = connect();
        // Query
        $sql = "INSERT INTO USERS(user, pwd) VALUES(?, ?);";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("ss", $user, $pss1);
        if ($stmt->execute()) {
            echo("yes");
        } else {
            echo "Falló el registro: (" . $stmt->errno . ") " . $stmt->error;
        }
        // Close connection
        $conn->close();
    } else  echo("Contraseñas no coinciden!!!");
} else  header("location: http://".$_SERVER['HTTP_HOST']."/mediapp/public/html/data-error.html");
?>