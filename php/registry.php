<?php
if (session_status() == PHP_SESSION_NONE)   session_start();
if (isset($_SESSION["id"])) {
    if (isset($_POST["id-exp"]) && isset($_POST["name"]) && isset($_POST["desc"]) && isset($_POST["can"])) {
        $exp = $_POST["id-exp"];
        $name = $_POST["name"];
        $desc = $_POST["desc"];
        $can = $_POST["can"];
        if ($can < 1) {
            echo("Cantidad incorrecta");
            return;
        }
        // Connection
        include_once("./connection.php");
        $conn = connect();
        // Query
        $sql = "INSERT INTO MEDICAMENTOS VALUES(?, ?, ?, ?);";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("isis", $exp, $name, $can, $desc);
        if ($stmt->execute()) {
            echo("yes");
        } else {
            echo("Falló el registro de item: (" . $stmt->errno . ") " . $stmt->error);
        }
        // Close connection
        $conn->close();
        return;
    } else  header("location: http://".$_SERVER['HTTP_HOST']."/mediapp/public/html/data-error.html");
} else  header("location: http://".$_SERVER['HTTP_HOST']."/mediapp/public/html/no-user.html");
?>