<?php
if (session_status() == PHP_SESSION_NONE)   session_start();
if(isset($_SESSION["id"])) {
    if(isset($_POST["exp"])) {
        $exp = $_POST["exp"] ;
        // Connection
        include_once("./connection.php");
        $conn = connect();
        // Query
        $sql = "DELETE FROM MEDICAMENTOS WHERE expediente=?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("i", $exp);
        if ($stmt->execute())   echo("yes");
        else    echo("Fallo al eliminar el item: (" . $stmt->errno . ") " . $stmt->error);
    } else  header("location: http://".$_SERVER['HTTP_HOST']."/mediapp/public/html/data-error.html");
} else  header("location: http://".$_SERVER['HTTP_HOST']."/mediapp/public/html/no-user.html");
?>