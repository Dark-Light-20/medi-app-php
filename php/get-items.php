<?php
header('Content-Type: application/json, charset=utf-8');
if (session_status() == PHP_SESSION_NONE)   session_start();
if (isset($_SESSION["id"])) {
    // Connection
    include_once("./connection.php");
    $conn = connect();
    // Query
    $sql = "SELECT * FROM MEDICAMENTOS;";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $result = $stmt->get_result();
    $items = [];
    // Check
    if ($result->num_rows > 0)  while($item = $result->fetch_assoc()) array_push($items, $item);
    echo(json_encode($items, JSON_UNESCAPED_UNICODE));
    // Close connection
    $conn->close();
} else  header("location: http://".$_SERVER['HTTP_HOST']."/mediapp/public/html/no-user.html");
?>
