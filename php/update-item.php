<?php
if (session_status() == PHP_SESSION_NONE)   session_start();
if(isset($_SESSION["id"])) {
    if(isset($_POST["op"]) && isset($_POST["cnd"]) && isset($_POST["exp"])) {
        $op = $_POST["op"];
        $cnd = $_POST["cnd"] ;
        $exp = $_POST["exp"] ;
        // Connection
        include_once("./connection.php");
        $conn = connect();
        // Query
        switch ($op) {
            case "Agregar":
                // Buy quantity
                $sql = "UPDATE MEDICAMENTOS SET cantidad=cantidad+? WHERE expediente=?";
                $stmt = $conn->prepare($sql);
                $stmt->bind_param("ii", $cnd, $exp);
                if ($stmt->execute())   echo("yes");
                else    echo("Fallo al actualizar cantidad de item: (" . $stmt->errno . ") " . $stmt->error);
                break;
            case "Vender":
                // Get actual quantity
                $check = "SELECT cantidad FROM MEDICAMENTOS WHERE expediente=?";
                $stmt = $conn->prepare($check);
                $stmt->bind_param("i", $exp);
                if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $cndAct = $result->fetch_assoc()["cantidad"];
                }
                else    {
                    echo("Fallo al consultar cantidad de item: (" . $stmt->errno . ") " . $stmt->error);
                    return;
                }
                // Check sell
                if ($cnd <= $cndAct) {
                    $sql = "UPDATE MEDICAMENTOS SET cantidad=cantidad-? WHERE expediente=?";
                    $stmt = $conn->prepare($sql);
                    $stmt->bind_param("ii", $cnd, $exp);
                    if ($stmt->execute())   echo("yes");
                    else    echo("Fallo al actualizar cantidad de item: (" . $stmt->errno . ") " . $stmt->error);
                }
                else    echo("La cantidad que trata de vender es mayor a la actual");
                break;
        }
        return;
    } else  header("location: http://".$_SERVER['HTTP_HOST']."/mediapp/public/html/data-error.html");
} else  header("location: http://".$_SERVER['HTTP_HOST']."/mediapp/public/html/no-user.html");
?>