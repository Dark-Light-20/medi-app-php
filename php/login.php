<?php
if (!isset($_POST["username"])) header("location: http://".$_SERVER['HTTP_HOST']."/mediapp");
$user = $_POST["username"]; 
$pass = $_POST["password"];
// Connection
include_once("./connection.php");
$conn = connect();
// Query
$sql = "SELECT ID, pwd FROM USERS WHERE user=?;";
$stmt = $conn->prepare($sql);
$stmt->bind_param("s", $user);
$stmt->execute();
$result = $stmt->get_result();
// Check
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    // Verify password
    if (password_verify($pass, $row["pwd"])) {
        session_start();
        $_SESSION["id"] = $row["ID"];
        $_SESSION["user"] = $user;
        $_SESSION["pass"] = $pass;
        echo($user);
    } else {
        echo("no");
    }
} else {
    echo("no");
}
// Close connection
$conn->close();
?>