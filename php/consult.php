<?php
if (session_status() == PHP_SESSION_NONE)   session_start();
if (isset($_SESSION["id"])) {
?>
    <!DOCTYPE html>
    <html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Inventario - MediApp</title>
        <link rel="stylesheet" type="text/css" href="/mediapp/public/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/mediapp/public/css/style.css">
        <link rel="stylesheet" type="text/css" href="/mediapp/public/DataTables/datatables.min.css"/>
        <link rel="stylesheet" type="text/css" href="/mediapp/public/DataTables/DataTables-1.10.21/css/dataTables.bootstrap4.min.css"/>
        <link rel="stylesheet" type="text/css" href="/mediapp/public/css/material-icons-config.css">
        <script src="/mediapp/public/js/fontawsome-config.js"></script>
    </head>
    <body>
        <!-- *Modals* -->
        <!-- Registry -->
        <div class="modal fade" id="modalRegistry" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="row justify-content-center text-center">
                        <div class="modal-header">
                            <h1><span class="badge badge-pill badge-info col">Agregar Item</span></h1>
                        </div>
                    </div>
                    <div class="modal-body" id="modalBodyRegistry">
                        <form id="registry-form" autocomplete="off">
                            <div class="form-group">
                                <label for="name">Nombre:</label>
                                <input type="text" id="name" class="form-control" name="name" placeholder="Nombre de item..." maxlength="50" required>
                            </div>
                            <div class="form-group">
                                <label for="id-exp">N° de expediente:</label>
                                <input type="text" id="id-exp" class="col-md-5 form-control" name="id-exp" placeholder="ID de item..." maxlength="10" required>
                            </div>
                            <div class="form-group">
                                <label for="desc">Descripción:</label>
                                <textarea type="text" id="desc" class="form-control" name="desc" placeholder="Detalle de item..." maxlength="255" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="can">Cantidad:</label>
                                <input type="number" id="can" class="col-md-5 form-control" name="can" placeholder="10" min="1" required>
                            </div>
                            <div id="msgRegistry"></div>
                        </form>
                        <div class="autocomplete list-group" id="list"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" id="btnRegistry">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add/Sell -->
        <div class="modal fade" id="modalAddSell" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title"><span id="modalTitle"></span></h1>
                    </div>
                    <div class="modal-body" id="modalBodyAddSell">
                        <h5>Item: Expediente # <span id="expTxt"></span></h5>
                        <form>
                            <div class="form-group">
                                <label for="modalNum" class="col-form-label">Cantidad:</label>
                                <input type="number" class="form-control col-4" name="modalNum" id="modalNum">
                            </div>
                        </form>
                        <div id="msgAddSell"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn" id="btnModalAddSell"></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Info -->
        <div class="modal fade" id="modalInfo" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Detalles de Item, Expediente: <span id="infoExp"></span></h1>
                    </div>
                    <div class="modal-body" id="modalBodyInfo">
                        <!-- Print Info -->
                        <h5>Producto: <span id="infoProd"></span></h5>
                        <h6>Titular: <span id="infoTitular"></span></h6>
                        <h6>Registro: <span id="infoReg"></span></h6>
                        <h6>Fecha de Expedición: <span id="infoFechEx"></span></h6>
                        <h6>Fecha de Vencimiento: <span id="infoFechVen"></span></h6>
                        <h6>Cantidad de contenido: <span id="infoCnd"></span></h6>
                        <h6>Descripción Comercial: <span id="infoDesc"></span></h6>
                        <h6>Via de administriación: <span id="infoVia"></span></h6>
                        <h6>Principio Activo: <span id="infoPrAc"></span></h6>
                        <h6>Unidad: <span id="infoUnd"></span></h6>
                        <h6>Unidad de medida: <span id="infoUndMd"></span></h6>
                        <h6>Unidad de referencia: <span id="infoUndRf"></span></h6>
                    </div>
                    <div id="msgInfo"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Delete -->
        <div class="modal fade" id="modalDel" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Eliminar Item</h1>
                    </div>
                    <div class="modal-body" id="modalBodyDel">
                        <h5>¿Está seguro de eliminar el Item # <span id="expTxtDel"></span> ?</h5>
                    </div>
                    <div id="msgDel"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-danger" id="btnModalDel">Eliminar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page -->
        <div id="navbar"></div>
        <div class="container" id="container">
            <div class="row justify-content-center text-center mt-3">
                <h1><span class="badge badge-pill badge-info">Inventario</span></h1>
            </div>
            <button class='btn btn-primary' data-toggle='modal' data-target='#modalRegistry'><i class='material-icons'>library_add</i>&nbsp;Agregar Item</button>
            <div class="row justify-content-center mt-3">
                <div class="col">
                    <div class="table-responsive mb-3">
                        <table id="data-table" class="table table-striped table-bordered" style="width:100%">
                            <thead class="text-center">
                                <tr>
                                    <th>Expediente</th>
                                    <th>Nombre</th>
                                    <th>Cantidad</th>
                                    <th>Descripción</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>  
            </div>
        </div>
        <script src="/mediapp/public/js/jquery-3.5.1.min.js"></script>
        <script src="/mediapp/public/js/popper.min.js"></script>
        <script src="/mediapp/public/js/bootstrap.min.js"></script>
        <script src="/mediapp/public/js/home.js"></script>
        <script src="/mediapp/public/DataTables/datatables.min.js"></script>
        <script src="/mediapp/public/js/table.js"></script>
    </body>
    </html>
<?php
} else  header("location: http://".$_SERVER['HTTP_HOST']."/mediapp/public/html/no-user.html");
?>