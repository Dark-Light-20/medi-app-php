<?php
if (session_status() == PHP_SESSION_NONE)   session_start();
if (isset($_SESSION["id"])) {
    unset($_SESSION["id"]);
    unset($_SESSION["user"]);
    unset($_SESSION["pass"]);
    session_destroy();
    header("location: http://".$_SERVER['HTTP_HOST']."/mediapp/public/html/logout.html");
} else  header("location: http://".$_SERVER['HTTP_HOST']."/mediapp");
?>