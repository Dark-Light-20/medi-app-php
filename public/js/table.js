$(document).ready(function () {
  var table = $("#data-table").DataTable({
    language: {
      url: "/mediapp/public/DataTables/Spanish.json",
    },
    ajax: {
      url: "/mediapp/php/get-items.php",
      dataSrc: "",
    },
    columns: [
      { data: "expediente" },
      { data: "nombre" },
      { data: "cantidad" },
      { data: "descripcion" },
      {
        defaultContent: `<div class='text-center'>
          <div class='btn-group btn-group-sm'>
          <button class='btn btn-success btnAdd' data-toggle='modal' data-target='#modalAddSell' data-type='add'><i class='material-icons'>add_circle</i></button>
          <button class='btn btn-warning btnSell' data-toggle='modal' data-target='#modalAddSell' data-type='sell'><i class='material-icons'>remove_circle</i></button>
          <button class='btn btn-info btnInfo' data-toggle='modal' data-target='#modalInfo'><i class='material-icons'>info</i></button>
          <button class='btn btn-danger btnDelete' data-toggle='modal' data-target='#modalDel'><i class='material-icons'>delete</i></button>
          </div>
          </div>`,
      },
    ],
    dom: "lBfrtip",
    buttons: [
      {
        extend: "copyHtml5",
        text: "<button class='btn btn-info'><i class='fas fa-copy'></button>",
        titleAttr: "Copy",
        exportOptions: { columns: [0, 1, 2, 3] },
      },
      {
        extend: "excelHtml5",
        text:
          "<button class='btn btn-success'><i class='fas fa-file-excel'></i></button>",
        titleAttr: "Excel",
        exportOptions: { columns: [0, 1, 2, 3] },
      },
      {
        extend: "pdfHtml5",
        text:
          "<button class='btn btn-danger'><i class='fas fa-file-pdf'></button>",
        titleAttr: "PDF",
        exportOptions: { columns: [0, 1, 2, 3] },
      },
    ],
  });

  // Registry Item
  $("#btnRegistry").on("click", () => {
    var registryData = new FormData($("#registry-form")[0]);
    $.ajax({
      type: "POST",
      url: "/mediapp/php/registry.php",
      data: registryData,
      processData: false,
      contentType: false,
      success: (data) => {
        if (data === "yes") {
          table.ajax.reload();
          $("#modalRegistry").modal("hide");
        } else {
          $("#msgRegistry").html(
            `<div class='alert alert-danger'>Server Error!!! ${data} </div>`
          );
        }
        $("#registry-form").trigger("reset");
      },
      error: function (xhr, status, error) {
        let errorMessage = `${xhr.status} : ${xhr.statusText}`;
        $("#msgRegistry").html(
          `<div class='alert alert-danger'>API Error!!! ${errorMessage} </div>`
        );
      },
    });
  });

  // Dynamic autocomplete Registry
  $("#modalRegistry").on("show.bs.modal", () => {
    $("#name").on("input", (e) => {
      const name = $(e.target).val().toUpperCase();
      if (name !== "" && name.length > 3) {
        $("#list").html("");
        $.ajax({
          type: "GET",
          url: `https://www.datos.gov.co/resource/i7cb-raxc.json?$query=select%20distinct%20expediente,producto%20where%20producto%20like%20%27${name}%25%27`,
          dataType: "json",
          success: (data) => {
            let items = "";
            $.each(data, (index, item) => {
              items += `<div class='class list-group-item list-group-item-action medi-item'>${item.producto} | Exp: ${item.expediente}</div>`;
            });
            $("#list").html(items);
          },
        });
      } else $("#list").html("");
    });
  });

  // Fill autocomplete item data
  $(document).on("click", ".medi-item", (e) => {
    $("#list").html("");
    let data = $(e.target).text().split(" | ");
    $("#name").val(data[0]);
    $("#id-exp").val(data[1].substring(5));
  });

  $("#modalRegistry").on("hide.bs.modal", () => {
    $("#registry-form")[0].reset();
  });

  // Dynamic change titles/buttons Add/Sell
  $("#modalAddSell").on("show.bs.modal", (e) => {
    let mType = $(e.relatedTarget).data("type");
    switch (mType) {
      case "add":
        $("#modalTitle").html("Agregar Unidades");
        $("#btnModalAddSell")
          .removeClass("btn-warning btn-danger")
          .addClass("btn-primary");
        $("#btnModalAddSell").html("Agregar");
        break;
      case "sell":
        $("#modalTitle").html("Vender Unidades");
        $("#btnModalAddSell")
          .removeClass("btn-primary btn-danger")
          .addClass("btn-warning");
        $("#btnModalAddSell").html("Vender");
        break;
    }
  });

  // Set IDs / reset Unities for Add/Sell
  $(document).on("click", ".btnAdd, .btnSell", (e) => {
    let row = $(e.target).closest("tr");
    exp = row.find("td:eq(0)").text();
    $("#expTxt").html(exp);
    $("#modalNum").val("");
    $("#msgAddSell").html("");
  });

  // Update Request
  $(document).on("click", "#btnModalAddSell", () => {
    let num = $("#modalNum").val();
    let type = $("#btnModalAddSell").html();
    $.ajax({
      type: "POST",
      url: "/mediapp/php/update-item.php",
      data: { op: type, cnd: num, exp: exp },
      success: (data) => {
        if (data === "yes") {
          table.ajax.reload();
          $("#modalAddSell").modal("hide");
        } else {
          $("#msgAddSell").html(
            `<div class='alert alert-danger'>Server Error!!! ${data} </div>`
          );
        }
      },
    });
  });

  // Load data for Info Modal
  $(document).on("click", ".btnInfo", (e) => {
    let row = $(e.target).closest("tr");
    exp = row.find("td:eq(0)").text();
    $("#infoExp").html(exp);
    // Request
    $.ajax({
      type: "GET",
      url: `https://www.datos.gov.co/resource/i7cb-raxc.json?expediente=${exp}`,
      data: { $limit: 1 },
      dataType: "json",
      success: (data) => {
        if (data.length > 0) {
          $("#infoProd").html(data[0]["producto"]);
          $("#infoTitular").html(data[0]["titular"]);
          $("#infoReg").html(data[0]["registrosanitario"]);
          $("#infoFechEx").html(data[0]["fechaexpedicion"]);
          $("#infoFechVen").html(data[0]["fechavencimiento"]);
          $("#infoCnd").html(data[0]["cantidadcum"]);
          $("#infoDesc").html(data[0]["descripcioncomercial"]);
          $("#infoVia").html(data[0]["viaadministracion"]);
          $("#infoPrAc").html(data[0]["principioactivo"]);
          $("#infoUnd").html(data[0]["cantidad"]);
          $("#infoUndMd").html(data[0]["unidadmedida"]);
          $("#infoUndRf").html(data[0]["unidadreferencia"]);
        } else {
          // Clear Fields Struct
          $("#modalBodyInfo").html("");
          $("#msgInfo").html(
            `<div class='alert alert-danger'>No se encontró información del item # ${exp}</div>`
          );
        }
      },
      error: function (xhr, status, error) {
        let errorMessage = `${xhr.status} : ${xhr.statusText}`;
        $("#msgInfo").html(
          `<div class='alert alert-danger'>API Error!!! ${errorMessage} </div>`
        );
      },
    });
  });

  $("#modalInfo").on("hide.bs.modal", () => {
    // Reload Fields Struct
    $("#msgInfo").html("");
    $.ajax({
      type: "GET",
      url: "/mediapp/public/html/infoFields.html",
      success: (data) => {
        $("#modalBodyInfo").html(data);
      },
    });
  });

  // Set ID for Del
  $(document).on("click", ".btnDelete", (e) => {
    let row = $(e.target).closest("tr");
    exp = row.find("td:eq(0)").text();
    $("#expTxtDel").html(exp);
  });

  // Delete Request
  $(document).on("click", "#btnModalDel", () => {
    $.ajax({
      type: "POST",
      url: "/mediapp/php/delete-item.php",
      data: { exp: exp },
      success: (data) => {
        if (data === "yes") {
          table.ajax.reload();
          $("#modalDel").modal("hide");
        } else {
          $("#msgDel").html(
            `<div class='alert alert-danger'>Server Error!!! ${data} </div>`
          );
        }
      },
    });
  });
});
