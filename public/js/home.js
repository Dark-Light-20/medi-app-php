$(document).ready(function () {
  // Load Navbar
  $.ajax({
    type: "GET",
    url: "/mediapp/public/html/navbar-user.html",
    success: (data) => {
      $("#navbar").html(data);
      $.ajax({
        type: "GET",
        url: "/mediapp/php/get-username.php",
        success: (data) => {
          if (data !== "no") $("#navbar-title").html("Bienvenido " + data);
        },
      });
    },
  });
});
