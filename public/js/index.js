$(document).ready(() => {
  // HTML Vars
  title = $("#title");
  container = $("#container");

  // Login-Home invoke function
  function login() {
    // Login AJAX
    $.ajax({
      type: "GET",
      url: "/mediapp/public/html/login.html",
      success: loginLoad,
      error: handleError,
    });
  }

  // Render login form
  function loginLoad(data) {
    title.html("Medi-App Login");
    container.html(data);

    // Form Submit
    $("#login-form").on("submit", (e) => {
      e.preventDefault();
      loginData = new FormData($("#login-form")[0]);
      $.ajax({
        type: "POST",
        url: "/mediapp/php/login.php",
        data: loginData,
        processData: false,
        contentType: false,
        success: loginSubmit,
        error: handleError,
      });
    });

    // Sign-in invoke function
    $("#btn-sigin").click(() => {
      $.ajax({
        type: "GET",
        url: "/mediapp/public/html/sigin.html",
        success: siginLoad,
        error: handleError,
      });
    });
  }

  // Render sigin form
  function siginLoad(data) {
    title.html("Medi-App Registry");
    container.html(data);
    $("#btnHome").click(login);

    // Form Submit
    $("#sigin-form").on("submit", (e) => {
      e.preventDefault();
      if ($("#password1").val() === $("#password2").val()) {
        var siginData = new FormData($("#sigin-form")[0]);
        $.ajax({
          type: "POST",
          url: "/mediapp/php/sigin.php",
          data: siginData,
          processData: false,
          contentType: false,
          success: siginSubmit,
          error: handleError,
        });
      } else {
        $("#log-container").html(
          "<div class='alert alert-danger' role='alert'>" +
            "Las contraseñas no coinciden!" +
            "</div>"
        );
      }
    });
  }

  // Sigin request result
  function siginSubmit(data) {
    if (data === "yes") {
      $.ajax({
        type: "GET",
        url: "/mediapp/public/html/success.html",
        success: (data2) => {
          container.html(data2);
          $("#btn-go-login").click(login);
        },
        error: handleError,
      });
    } else {
      $("#log-container").html(
        "<div class='alert alert-danger' role='alert'>" +
          "Error en los datos al registrar!" +
          "</div>"
      );
    }
  }

  // Login request result
  function loginSubmit(data) {
    if (data !== "no") {
      window.location.href = "/mediapp/php/consult.php";
    } else {
      $("#log-container").html(
        "<div class='alert alert-danger' role='alert'>" +
          "Datos incorrectos!" +
          "</div>"
      );
    }
  }

  // AJAX error log function
  function handleError(jqXHR, textStatus, error) {
    console.log(error);
  }

  login();
});
